/*
 * at24cxx.h
 *
 *  Created on: Nov 26, 2023
 *      Author: Daniel Meneses
 */

#ifndef INC_AT24CXX_H_
#define INC_AT24CXX_H_

#include "i2c.h"

#define AT24CXX_ADDR 0xA0

typedef enum
{
	AT24CXX_OK,
	AT24CXX_BUSY,
	AT24CXX_ERROR
}at24cxx_status_t;

typedef union
{
	struct
	{
		uint16_t byteAddr 	:6;
		uint16_t pageAddr	:9;
		uint16_t dummy		:1;
	}memoryAddr;
	uint16_t memoryAddrWord;
}at24cxx_memoryAddr_t;

at24cxx_status_t at24cxx_memoryByteWrite(at24cxx_memoryAddr_t *memAddr, uint8_t dataByte);
at24cxx_status_t at24cxx_memoryPageWrite(at24cxx_memoryAddr_t *memAddr, uint8_t *dataByte);
at24cxx_status_t at24cxx_memoryCurrentAddrRead(uint8_t *dataByte);
at24cxx_status_t at24cxx_memorySeqRead(at24cxx_memoryAddr_t *memAddr, uint8_t *dataByte, size_t len);
at24cxx_status_t at24cxx_memoryReadSector(uint8_t *sectorBuff, uint32_t sectorAddr, size_t len);
at24cxx_status_t at24cxx_memoryWriteSector(uint8_t *sectorBuff, uint32_t sectorAddr, size_t len);
#endif /* INC_AT24CXX_H_ */
