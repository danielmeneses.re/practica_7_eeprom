/*
 * at24cxx.c
 *
 *  Created on: Nov 26, 2023
 *      Author: Daniel Meneses
 */

#include "at24cxx.h"
#include <stdlib.h>
#include <string.h>

static at24cxx_status_t at24cxx_write(uint16_t addr, uint8_t *pData, size_t len)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	HAL_StatusTypeDef hal_status;
	uint8_t *dataWrite = malloc(70);
	memset(dataWrite, (int)(NULL), 70);
	*dataWrite = (addr & 0xFF00) >> 8;
	dataWrite++;
	*dataWrite = addr & 0xFF;
	dataWrite++;

	memcpy(dataWrite, pData, len);
	dataWrite -= 2;
	hal_status = HAL_I2C_Master_Transmit(&hi2c1, AT24CXX_ADDR, dataWrite, len+2, 100);

	if (hal_status == HAL_OK)
	{
		status = AT24CXX_OK;
	}
	else if (hal_status == HAL_BUSY)
	{
		status = AT24CXX_BUSY;
	}
	return status;
}

static at24cxx_status_t at24cxx_read(uint16_t addr, uint8_t *pData, size_t len)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	HAL_StatusTypeDef hal_status;
	uint8_t auxAddr[2];
	auxAddr[0] = (addr & 0xFF00) >> 8;
	auxAddr[1] = addr & 0xFF;
	hal_status = HAL_I2C_Master_Transmit(&hi2c1, AT24CXX_ADDR, auxAddr, 2, 100);
	if (hal_status == HAL_OK)
	{
		hal_status = HAL_I2C_Master_Receive(&hi2c1, AT24CXX_ADDR, pData, len, 100);
		if (hal_status != HAL_OK)
		{
			/*TODO: implementar manejador de errores */
		}
		else{
			status = AT24CXX_OK;
		}
	}
	else
	{
		/*TODO: implementar manejador de errores */
	}
	return status;
}

at24cxx_status_t at24cxx_memoryByteWrite(at24cxx_memoryAddr_t *memAddr, uint8_t dataByte)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	status = at24cxx_write(memAddr->memoryAddrWord, &dataByte, 1);

	if(status != AT24CXX_OK)
	{
		/*TODO: implementar manejador de errores */
	}
	return status;
}

at24cxx_status_t at24cxx_memoryPageWrite(at24cxx_memoryAddr_t *memAddr, uint8_t *dataByte)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	if(memAddr->memoryAddr.byteAddr !=0)
	{
		/*TODO: implementar manejador de errores */
		return status;
	}
	status = at24cxx_write(memAddr->memoryAddrWord, dataByte, 64);

	if(status != AT24CXX_OK){
		/*TODO: implementar manejador de errores */
	}
	return status;
}

at24cxx_status_t at24cxx_memoryCurrentAddrRead(uint8_t *dataByte)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	HAL_StatusTypeDef halStat;
	halStat = HAL_I2C_Master_Receive(&hi2c1, AT24CXX_ADDR, dataByte, 1, 100);
	if(halStat != HAL_OK){
		/*TODO: implementar manejador de errores */
	}
	else{
		status = AT24CXX_OK;
	}
	return status;
}

at24cxx_status_t at24cxx_memorySeqRead(at24cxx_memoryAddr_t *memAddr, uint8_t *dataByte, size_t len)
{
	at24cxx_status_t status = AT24CXX_ERROR;
	at24cxx_read(memAddr->memoryAddrWord, dataByte, len);
	return status;
}

at24cxx_status_t at24cxx_memoryReadSector(uint8_t *sectorBuff, uint32_t sectorAddr, size_t len){
	at24cxx_status_t status = AT24CXX_ERROR;
	at24cxx_memoryAddr_t auxAddr;
	auxAddr.memoryAddr.byteAddr = 0;
	auxAddr.memoryAddr.pageAddr = sectorAddr * 8;
	at24cxx_memorySeqRead(&auxAddr, sectorBuff, 512);
	return status;
}

at24cxx_status_t at24cxx_memoryWriteSector(uint8_t *sectorBuff, uint32_t sectorAddr, size_t len){
	at24cxx_status_t status = AT24CXX_ERROR;
	return status;
}
